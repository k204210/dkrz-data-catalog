# DKRZ data catalog

The DKRZ data catalog is a bottom-up collection of catalogs generated by DKRZ data hosts to allow DKRZ users to find and make use of their data.

The DKRZ data catalog repository contains all tools to create, host and build services around the dkrz data catalog.

## Features

- Catalog entries become visible and machine-readable for the whole world
- Data sources can be easily configured so that they can be easily loaded into the work environment
- All DKRZ users can contribute to make their data more [*FAIR*]()
- Both self-maintenance and automated services are enabled to collect and update catalogs

Become part of the DKRZ data catalog and extend your potential data user community!

## Usage

The repository is fully open so that it is possible to load the catalog from the web via gitlab´s raw link. 

### APIs and GUIs

- You can open the catalog via `intake.open_catalog("https://gitlab.dkrz.de/data-infrastructure-services/intake-catalog/-/raw/main/dkrz.yaml")`. It is planned to set a short link for that in near future.
- This repo is also cloned under `/pool/data/catalog` on Levante's file system

## Design

Catalogs are *yaml* files that should be openable by a catalog software tool like [intake](https://intake.readthedocs.io/en/latest/). They are nested in a flat hierarchy and should point to sub- and parent catalogs.

At top level, it distinguishes between

- data lake: An unordered, open collection of data sources that allows easy ingestion of new data sources by merge requests but can contain redundancies.
- data pool: A mirror of the structure of DKRZ´s data pool (/pool/data) which comes with both requirements set by the pool/data services and support by catalog services which exploit the requirements to harvest catalogs.

## Contribute

- Data lake:
    - Open a merge request and set a pointer to another catalog in another DKRZ gitlab repository.
    - Requirements:
        - Specify in the metadata section
            - account (example: bm1344)
            - part_of (example: MPI-M, NextGEMs,ICON)
        - Each maintainer of a subcatalog is responsible for its content
        - Referenced subcatalogs are in the institutional [gitlab](gitlab.dkrz.de)

- Data pool: Configure your catalog so that it can be harvested by the catalog *services* explained below.

### Service

> scripts/create_pool_catalog.ipynb

- For /pool/data, we are mirroring the directory structure with a script. For each subdirectory, a crawler searches for `/pool/data/SUBDIR/main.yaml` and adds it and all subcatalogs to dkrz's catalog if requirements are fulfilled
- All WLA projects can become part of /pool/data by requesting a link to their data directory on the HPC. Requirements for linked data are:
    - Readable data
    - A README file `/pool/data/SUBDIR/README*` including license
- You can open the catalog with `intake.open_catalog(https://gitlab.dkrz.de/data-infrastructure-services/intake-catalog/-/raw/main/dkrz.yaml)["pool"]`

## Notes

- This repo replaces the old intake-esm repository
